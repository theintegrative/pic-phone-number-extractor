## 0.4.3 (2024-05-10)

### Fix

- remove type keyword instead

### Refactor

- make code more reusable

## 0.4.2 (2024-05-09)

### Fix

- use string literal for f-string

## 0.4.1 (2024-05-09)

### Fix

- write phone numbers on each line

## 0.4.0 (2024-05-09)

### Feat

- add reporting in txt, json and csv

## 0.3.0 (2024-05-09)

### Feat

- make processing multi threaded
- add progress bar for directories

### Refactor

- use poetry to manage dependencies

## 0.2.0 (2024-05-08)

### Feat

- add cli interface and dir or file detection on image extention
