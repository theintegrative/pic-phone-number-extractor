import csv
import json
import phonenumbers
import pytesseract
import typer
import concurrent.futures
from PIL import Image
from pathlib import Path
from oiext.util import OptionFactory, OptionFunc


# Settings
pytesseract.pytesseract.tesseract_cmd = r"/usr/bin/tesseract"
image_ext = [".jpg", ".jpeg", ".png", ".gif", ".bmp"]
output_ext = [".txt", ".json", ".csv", ".vcf"]
number_format = phonenumbers.PhoneNumberFormat.E164


class ReportNumbers:
    def __init__(self):
        self.reports = OptionFactory()

    def txt(self, results, output_file):
        with open(output_file, "w") as file:
            [file.write(result["number"] + "\n") for result in results]

    def json(self, results, output_file):
        with open(output_file, "w") as file:
            json.dump(results, file, indent=4)

    def csv(self, results, output_file):
        keys = results[0].keys() if results else []
        with open(output_file, "w", newline="") as file:
            writer = csv.DictWriter(file, fieldnames=keys)
            writer.writeheader()
            writer.writerows(results)

    def vcf(self, results, output_file):
        with open(output_file, "w") as file:
            for contact in results:
                file.write("BEGIN:VCARD\n")
                file.write("VERSION:2.1\n")
                file.write(f"TEL;CELL:{contact['number']}\n")
                file.write("END:VCARD\n")

    def option(self, name) -> OptionFunc:
        self.reports.add(".txt", self.txt)
        self.reports.add(".json", self.json)
        self.reports.add(".csv", self.csv)
        self.reports.add(".vcf", self.vcf)
        if not (reporter := self.reports.option(name)):
            return print(f"Invalid option: {name}")
        return reporter


class NumberExtractor:
    def __init__(self, path: str, output: str):
        self.path = Path(path)
        self.output = Path(output)
        if not self.path.exists():
            typer.echo("Input path does not exist.")
            raise typer.Exit(code=1)

    def format(self, match: str, path: Path):
        return {
            "path": str(path.resolve()),
            "country": phonenumbers.region_code_for_number(match.number),
            "number": phonenumbers.format_number(match.number, number_format)
        }

    def extract(self, path: Path):
        image = Image.open(path)
        text = pytesseract.image_to_string(image)
        extracted = phonenumbers.PhoneNumberMatcher(text, None)
        results = []
        for match in extracted:
            results.append(self.format(match, path))
        return results

    def process(self):
        processed = []
        if self.path.is_file():
            if self.path.suffix.lower() in image_ext:
                processed.extend(self.extract(self.path))
        elif self.path.is_dir():
            with concurrent.futures.ProcessPoolExecutor() as executor:
                futures = []
                for file in self.path.glob("**/*"):
                    if file.is_file() and file.suffix.lower() in image_ext:
                        future = executor.submit(self.extract, file)
                        futures.append(future)
                    for future in concurrent.futures.as_completed(futures):
                        processed.extend(future.result())

        report = ReportNumbers()
        reporter = report.option(self.output.suffix.lower())
        reporter(processed, self.output)
