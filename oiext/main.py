import pytesseract
import typer
from oiext import number

# Settings
pytesseract.pytesseract.tesseract_cmd = r"/usr/bin/tesseract"
command = "oiext"
app = typer.Typer()


@app.command(help="Osint Image EXTractor")
def main(
    path: str = typer.Argument(help=f"Path to image(s): {number.image_ext}."),
    output: str = typer.Argument(
        default="output.txt", help=f"Path to output: {number.output_ext}"
    ),
):
    extractor = number.NumberExtractor(path, output)
    extractor.process()
    typer.echo(f"Results saved to {output}.")


if __name__ == "__main__":
    typer.run(main)
