from typing import Callable

OptionFunc = Callable[[list[str], str], str]


class OptionFactory:
    def __init__(self):
        self.options = {}

    def add(self, name: str, processor: OptionFunc) -> OptionFunc:
        self.options[name] = processor

    def option(self, name: str) -> OptionFunc:
        return self.options.get(name)
